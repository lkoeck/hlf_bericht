% --------------------------------
% Berechnungen zur Prädikativen Instandhaltung
% --------------------------------
%% Produktionszenario
% Aktive Zeit pro Zyklus
aZ = 360; %s
% Zyklen pro Stunde
zyklen = 0.5; % nur alle 2 h
% Anzahl aktive Stunden im Tag
aH = 24;
% Anzahl aktive Tage im Woche
aD = 7; 
% Anzahl aktive Wochen im Jahr
aW = 50;
% Anzahl aktive Jahre
aY = 6;
% Zeit aktiv
tA = aZ * zyklen * aH * aD * aW * aY / 3600; % h
%% Akku: PiJuice
 %Zuverlässigkeit des Akkus R(t)
 beta = 11.17;
 eta = 92678;
 
 R_akku = exp(-tA/eta)^beta;
%% OPV
%20200424_Modul8_Ausfallsraten_berp
lambda_opv =148.851e-9;

R_opv = exp(-lambda_opv*tA);
%% ADC
%vgl opv
%% Widerstände
%MIL_HDBK_217f
lambdab_res = 0.0065; %Ta = 100, Stress = 0.5
f_quality = 1; %Medium
f_environment = 8; %Ground Mobile
f_res = 1.1; % >0.1M to 1M

lambda_res = lambdab_res * f_quality *f_environment * f_res * 10e-6;

R_res = exp(-lambda_res*tA);
%% PTC
%NPRD-91
lambda_ptc = 0.0277e-6; %NPRD-91

R_ptc =  exp(-lambda_ptc*tA);

%% Gesamt Zuverlässigkeit
R =  R_akku * R_opv * R_opv * R_res * R_res * R_res * R_ptc;