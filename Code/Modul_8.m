% --------------------------------
% Berechnungen zur Prädikativen Instandhaltung
% --------------------------------

%% Daten
% Lamda Linear Zylinder
lamdaLZ = 5.1850e-6; %1/h
% Lamda Drehzylinder
lamdaDZ = 64.6302e-6; %1/h
% Lamda Näherungsschalter
lamdaNS = 0.5899e-6; %1/h
% Lamda Lichttaster
lamdaLT = 4.5065e-6; %1/h

% Aktive Zeit pro Zyklus
aZ = 20; %s
% Zyklen pro Stunde
zyklen = 10; 
% Anzahl aktive Stunden im Tag
aH = 24;
% Anzahl aktive Tage im Woche
aD = 7; 
% Anzahl aktive Wochen im Jahr
aW = 50;
% Anzahl aktive Jahre
aY = 6;

%% Berechnungen
% Zeit aktiv
tA = aZ * zyklen * aH * aD * aW * aY / 3600; % h
% Zuverlässigkeit Linear Zylinder
R_LZ = exp(-lamdaLZ*tA);
% Zuverlässigkeit Drehzlinder
R_DZ = exp(-lamdaDZ*tA);
% Zuverlässigkeit Näherungsschalter
R_NS = exp(-lamdaNS*tA);
% Zuverlässigkeit Lichttaster
R_LT = exp(-lamdaLT*tA);

% Zuverlässigkeit BE1
R_BE1 = R_DZ * R_NS * R_NS * R_LT;
% Zuverlässigkeit BE2
R_BE2 = R_LZ * R_NS * R_NS;
% Zuverlässigkeit BE3
R_BE3 = R_LZ * R_NS;

%% Gesamtzuverlässigkeit
R = R_BE1 * R_BE2 * R_BE3;
