% --------------------------------
% Berechnungen zur Prädikativen Instandhaltung
% --------------------------------

%% Daten
% Lamda Drosselrückschlagventil
lamdaDRV =4.176e-6; %1/h
% Lamda Drucksensor
lamdaDS = 23.2558e-6; %1/h


% Aktive Zeit pro Zyklus
aZ = 30; %s
% Zyklen pro Stunde
zyklen = 10; 
% Anzahl aktive Stunden im Tag
aH = 24;
% Anzahl aktive Tage im Woche
aD = 7; 
% Anzahl aktive Wochen im Jahr
aW = 50;
% Anzahl aktive Jahre
aY = 30;

%% Berechnungen
% Zeit aktiv
tA = aZ * zyklen * aH * aD * aW * aY / 3600; % h
% Zuverlässigkeit Drosselrückschlagventil
R_DRV = exp(-lamdaDRV*tA);
% Zuverlässigkeit Drucksensor
R_DS = exp(-lamdaDS*tA);


%% Gesamtzuverlässigkeit
R = R_DS * R_DRV * R_DRV;