%% Odometry calculation
% Leona K�ck, Chris R�ttimann
clc; clear all; close all;

%% Available measured data:
% Odometry calculated internally by robotino: x, y, phi
% Motor actual speed (wheels): w1_rpm, w2_rpm, w3_rpm
% Motor actual position (wheels): phi1_rev, phi2_rev, phi3_rev
% Time

load('dataset_robotino.mat')

%% Robot parameters
wheel_radius = 0.12/2; % [m]
robot_radius = 0.36/2; % [m]

wheel_phi1 = pi/3;    % [rad]
wheel_phi2 = pi;      % [rad]
wheel_phi3 = 5/3*pi;  % [rad]

i = 32; % Gear reduciton (Gear 1:8. Belt 1:4)

%% Odometry calculation (speed)

v_wheels = [wheel_radius*2*pi*w1_rpm/(i*60); wheel_radius*2*pi*w2_rpm/(i*60); wheel_radius*2*pi*w3_rpm/(i*60)];

%% Odometry calculation (position)
% Integral of above calculated speed

x_b = 0;
y_b = 0;
phi(1)= pi/3;

for n=1:length(time)-1
    
    dt = time(n+1)-time(n);
    
    phin = phi(n);
    
    M = 2/3 * [ -sin(phin) -cos(phin+pi/6)  cos(pi/6-phin);
                cos(phin) -sin(phin+pi/6) -sin(pi/6-phin);
                1/(2*robot_radius) 1/(2*robot_radius) 1/(2*robot_radius)];
            
    % Berechne die einzelnen Geschwindigkeitskomponenten        
    Res = M * [v_wheels(1,n); v_wheels(2,n); v_wheels(3,n)];
    
    % Berechne n�chste Koordinaten
    x_b(n+1) = x_b(n)+Res(1) * dt;
    y_b(n+1) = y_b(n)+Res(2) * dt;
    phi(n+1) = phi(n)+Res(3) * dt;
end

%% Plot Data
% x, y from robotino's odometry
% w1,2,3 angular speeds motors
% velocity odometry (calculated in matlab)
% position odometry (calculated in matlab)
% comparison (x,y) robotino vs. (x,y) odometry matlab
figure(1)
plot(x_b,y_b,'DisplayName','Berechnet')
title('2D')

axis equal
grid on
xlabel('x [m]')
ylabel('y [m]')
legend

hold on
plot(x,y,'DisplayName','Original')

figure(2)
plot(time,phi*180/pi-60)
title('Orientierung')
xlabel('t [s]')
ylabel('phi [�]')

figure(3)
plot(time,x-x_b,'DisplayName','x Diff')
title('Diff Original-Berechnet')
hold on
plot(time,y-y_b,'DisplayName','y Diff')
xlabel('t [s]')
ylabel('diff [m]')
legend